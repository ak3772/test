//
//  SimpleLayoutViewController.swift
//  SnapKit
//
//  Created by Spiros Gerokostas on 01/03/16.
//  Copyright © 2016 SnapKit Team. All rights reserved.
//

import UIKit
import SnapKit



class SimpleLayoutViewController: UIViewController {
    
    @IBOutlet weak var superView: UIView!
    

    
    var didSetupConstraints = false
    
    var viewArray = [UIView]()
    
    let blackView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        return view
    }()
    
    let redView: UIView = {
        let view = UIView()
        view.backgroundColor = .red
        return view
    }()
    
    let yellowView: UIView = {
        let view = UIView()
        view.backgroundColor = .yellow
        return view
    }()
    
    let blueView: UIView = {
        let view = UIView()
        view.backgroundColor = .blue
        return view
    }()
    
    let greenView: UIView = {
        let view = UIView()
        view.backgroundColor = .green
        return view
    }()
    
    let postWidth = 100
    
    var rowNumber = 0
    
    var randomHeight = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        
        
        //var postsOnCurrentRow = 0
        
        view.addSubview(blackView)
        view.addSubview(redView)
        view.addSubview(yellowView)
        view.addSubview(blueView)
        view.addSubview(greenView)
        
        view.setNeedsUpdateConstraints()
    }
    

    
    func moveView(oldView: UIView, newView: UIView, newHeight: CGFloat) {
        
        let postsPerRow = Int(superView.frame.width - 20) / Int(postWidth + 20)
        
        if (/*checkIntersection(newView: oldView, newHeight: newHeight)*/
            oldView.tag != 0) {
            moveView(oldView: viewArray[viewArray.index(of: oldView)! - 1] , newView: oldView, newHeight: oldView.frame.height)
        }
        
        let futureFrame = CGRect(x: oldView.frame.maxX + 20, y: oldView.frame.minY, width: oldView.frame.width, height: oldView.frame.height)
        
        if (!superView.bounds.contains(futureFrame)) {
            oldView.snp.remakeConstraints{ make in
                self.view.addSubview(oldView)
                
                if (rowNumber == 0) {
                make.top.equalTo(CGFloat(randomHeight)).offset(40)
                }
                else {
                make.top.equalTo(viewArray[viewArray.index(of: oldView)! + postsPerRow].snp.bottom).offset(20)
                }
                
                make.left.equalTo(self.view).offset(20)
                make.right.equalTo(oldView.snp.left).offset(100)
                make.bottom.equalTo(oldView.snp.top).offset(Int(newHeight))
                make.size.equalTo(CGSize(width:100, height:Int(newHeight)))
            }
            oldView.setNeedsLayout()
            UIView.animate(withDuration: 1.0, animations: {
                self.view.layoutIfNeeded()
            })
            
            oldView.frame = CGRect(x: 20, y: 40 + CGFloat(randomHeight), width: 100, height: oldView.frame.height)
            rowNumber += 1
        }
        else {
            oldView.snp.remakeConstraints{ make in
                self.view.addSubview(oldView)

                make.top.equalTo(futureFrame.minY)//.offset(20)
                make.left.equalTo(futureFrame.minX)//.offset(120)
                make.right.equalTo(oldView.snp.left).offset(100)
                make.bottom.equalTo(oldView.snp.top).offset(Int(newHeight))
                make.size.equalTo(CGSize(width:100, height:Int(newHeight)))
            }
            oldView.setNeedsLayout()
            UIView.animate(withDuration: 1.0, delay: 0, options: [.overrideInheritedCurve, .overrideInheritedOptions], animations: {
                self.view.layoutIfNeeded()
            })
            oldView.frame = futureFrame
        }
        
//        oldView.setNeedsLayout()
//        UIView.animate(withDuration: 1.0, animations: {
//
//            oldView.superview?.layoutIfNeeded()
//            self.view.layoutIfNeeded()
//        })
    }
    
    func checkIntersection(newView: UIView, newHeight: CGFloat) -> Bool {
        
        var futureFrame = CGRect(x: newView.frame.maxX + 20, y: newView.frame.minY, width: newView.frame.width, height: newHeight)
        
        if (!superView.bounds.contains(futureFrame)) {
            futureFrame = CGRect(x: 20, y: newView.frame.maxY + 20, width: 100, height: newHeight)

        }
            for aView in viewArray {
                if (newView.tag != aView.tag) {
                    if (futureFrame.equalTo(aView.frame) || futureFrame.intersects(aView.frame)) {
                        print("Yes1")
                        greenView.backgroundColor = .orange
                        return true
                                        }
                }
            }
        return false
        
    }
    
    func addNewViewContraints(newView: UIView) {
        
        randomHeight = Int(arc4random_uniform(201)) + 100
        
        if (newView.tag != 0) {
            moveView(oldView: viewArray[viewArray.index(of: newView)! - 1], newView: newView, newHeight: CGFloat(randomHeight))

        }
        
                UIView.animate(withDuration: 1.0, animations: {
        newView.snp.makeConstraints{make in
            
            self.view.addSubview(newView)

            make.top.equalTo(self.view).offset(80)
            make.left.equalTo(self.view).offset(20)
            make.right.equalTo(newView.snp.left).offset(100)
            make.bottom.equalTo(newView.snp.top).offset(Int(self.randomHeight))
            make.size.equalTo(CGSize(width:100, height:Int(self.randomHeight)))
            
        }
            newView.superview?.layoutIfNeeded()
        })
        //newView.frame = CGRect(x: 20, y: 80, width: 100, height: 100)
        //super.updateViewConstraints()
        

        


    }
    var number = 0
    var greenBlue = true
    
    
    @IBAction func addNewBox(_ sender: Any) {
        let newView: UIView = {
            let view = UIView()
            if greenBlue {
               view.backgroundColor = .green
            }
            else {view.backgroundColor = .blue}
            greenBlue = !greenBlue
            view.tag = number
            number += 1
            return view
        }()
        
        viewArray.append(newView)
        
        
        addNewViewContraints(newView: newView)
        
    }
    
}




//
//  BasicUIScrollViewController.swift
//  SnapKit
//
//  Created by Spiros Gerokostas on 01/03/16.
//  Copyright © 2016 SnapKit Team. All rights reserved.
//

//import UIKit
//import SnapKit
//
//class BasicUIScrollViewController: UIViewController {
//    
//    var didSetupConstraints = false
//    
//    let scrollView  = UIScrollView()
//    let contentView = UIView()
//    
//    let label: UILabel = {
//        let label = UILabel()
//        label.backgroundColor = .blue
//        label.numberOfLines = 0
//        label.lineBreakMode = .byClipping
//        label.textColor = .white
//        label.text = NSLocalizedString("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", comment: "")
//        return label
//    }()
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        view.backgroundColor = UIColor.white
//        
//        view.addSubview(scrollView)
//        
//        contentView.backgroundColor = UIColor.lightGray
//        scrollView.addSubview(contentView)
//        contentView.addSubview(label)
//        
//        view.setNeedsUpdateConstraints()
//    }
//    
//    override func updateViewConstraints() {
//        
//        if (!didSetupConstraints) {
//            
//            scrollView.snp.makeConstraints { make in
//                make.edges.equalTo(view).inset(UIEdgeInsets.zero)
//            }
//            
//            contentView.snp.makeConstraints { make in
//                make.edges.equalTo(scrollView).inset(UIEdgeInsets.zero)
//                make.width.equalTo(scrollView)
//            }
//            
//            label.snp.makeConstraints { make in
//                make.top.equalTo(contentView).inset(20)
//                make.leading.equalTo(contentView).inset(20)
//                make.trailing.equalTo(contentView).inset(20)
//                make.bottom.equalTo(contentView).inset(20)
//            }
//            
//            didSetupConstraints = true
//        }
//        
//        super.updateViewConstraints()
//    }
//}
